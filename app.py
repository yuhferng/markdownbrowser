# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from os import listdir
from os.path import isfile, join

external_stylesheets = ['https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.18.1/styles/night-owl.min.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

# colors = {
#     'background': '#111E22',
#     'text': '#FEFEFF'
# }

colors = {
    'background': '#FEFFEE',
    'text': '#000000'
}

# markdown_text = '''
# ### Dash and Markdown

# Dash apps can be written in Markdown.
# Dash uses the [CommonMark](http://commonmark.org/)
# specification of Markdown.
# Check out their [60 Second Markdown Tutorial](http://commonmark.org/help/)
# if this is your first introduction to Markdown!
# '''

with open('data/markdown1.md', 'r') as file:
    markdown_text = file.read()

server = app.server

mypath = "./data"
files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
tags = ["Linux", "Python", "Mac", "iPhone", "Android", "Windows"]
print(files)

app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    html.H1(
        children='Markdown Data Browser',
        style={
            'textAlign': 'center',
            'color': colors['text']
        }
    ),

    html.Div(children='Browse, View, Tag Markdown Archives', style={
        'textAlign': 'center',
        'color': colors['text']
    }),

    dcc.Input(id='my-id', value='', type='text'),

    dcc.Dropdown(
    options=[
        {'label': i, 'value': i} for i in tags
    ],
    value=['Linux'],
    multi=True
    ),
    
    dcc.Dropdown(
        id='select-doc',
        options=[
            {'label': i, 'value': i} for i in files
        ],
        value='files[0]'),

    html.Div(id='my-div'),
    dcc.Markdown(id='main_markdown',
        children=markdown_text,
        style={'color': colors['text']}
        ),

    # dcc.Markdown(children=markdown_text),


    dcc.Graph(
        id='example-graph-2',
        figure={
            'data': [
                {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
                {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
            ],
            'layout': {
                'plot_bgcolor': colors['background'],
                'paper_bgcolor': colors['background'],
                'font': {
                    'color': colors['text']
                }
            }
        }
    )
])

@app.callback(
    Output('main_markdown', 'children'),
    [Input('select-doc', 'value')]
)
def update_output_div(input_value):
    try:
        with open('data/' + input_value, 'r') as file:
            markdown_text = file.read()
    except:
        markdown_text = "Please type filename"
        # showing the file listing interface for selecting
    return markdown_text


# It will also depend on tags
@app.callback(
    Output('select-doc', 'options'),
    [Input('my-id', 'value')]
)
def update_output_div(input_value):
    try:
        options = [f for f in files if (input_value in f)]
        if options == None:
            options = []
    except:
        options = files
    
    return  [{'label': i, 'value': i} for i in options]

if __name__ == '__main__':
    app.run_server(debug=True)